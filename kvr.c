// A short exercise (RPG) written C
#include <stdio.h>

char characterName[64];
int potionCount = 10;
int healPower = 500;

int BattleCommand(char choice, int userATK, int userHP);
void BattleCommandText();
void VictoryText();
void GameOverText();

struct battlestats
{
	int DPR;
	int HP;
};

int main(int argc, char const *argv[])
{
	struct battleStats user;
		user.DPR;
		user.HP = 1000;

	struct battleStats dragon;
		dragon.DPR;
		dragon.HP = 5000;

	int critChance, critMultiplier;

	printf("Please enter desired name: ");
	fgets(characterName, 64, stdin);
	printf("\n");



	while(user.HP > 0 || dragon.HP > 0) {

		printf("The dragon has %d remaining life!\n", dragon.HP);
		printf("You have %d remaining HP\n", user.HP);

		critChance = rand() % 5;
		critMultiplier = 1;

		if(critChance > 4) {
			critMultiplier = 2;
		}

		dragon.DPR = ((rand() % 50 + 15) * critMultiplier);
		user.HP -= dragon.DPR;
		printf("The dragon has damaged you for %d\n", dragon.DPR);

		BattleCommandText();

		scanf("%c", &choice);
		printf("\n");

		BattleCommand(choice, user.DPR, user.HP);
		dragon.HP -= user.DPR;

		printf("You have dealt %d damage to the dragon!\n", user.DPR);
	}

	if(UserHP > DragonLife) {
		VictoryText();
	} else {
		GameOverText();
	}

	return 0;
}

int BattleCommand(char c, int atkd, int hp)
{
	extern int potionCount;
	extern int healPower = 500;
	char Choice;
	char fleeAlert;
	switch(c) {
		// Direct Attack.
		case 'A':
			atkd = rand () % 99 + 7;
			return atkd;
			break;
		// Heal
		case 'X':
			if(PotionCount > 0) {
				PotionCount--;
				hp += healPower;
				return hp;
				printf("Your HP was increased by %d, and you have %d remaining potions!\n", healPower, potionCount);
			} else {
				printf("You have no potions left!\n")
			}
			break;
		// Flee
		case 'F':
			printf("Are you sure to flee from battle?\n");
			scanf("%c", &fleeAlert);
			if(fleeAlert == 'Y' || fleeAlert == 'y'){
				GameOverText();
				SYSTEM("PAUSE");
			} else {
				printf("Select a command!\n")
				scanf("%c", &command);
				printf("\n");
				BattleCommand(command, atkd, hp);
				break;
			}
			break;
		// Repeats selection if choice was invalid.
		default:
			printf("You did not enter a valid command! Please enter your option again!\n");
			scanf("%c", &command);
			printf("\n");
			BattleCommand(command, atkd, hp);
			break;
	}
}

void BattleCommandText()
{
		printf("Select an option to defeat the dragon:\n");
		printf("A: Directly attack the dragon!\n");
		printf("X: Use a potion!\n");
		printf("F: Flee from the dragon!\n");
}

void VictoryText()
{
		printf("Congratulations! You have defeated the dragon! \n");
		printf("You shall be hailed as the dragon slayer!\n");
		printf("%s, The Dragon Slayer!\n", characterName);
}

void GameOverText()
{
		int loop;
		int command;
		printf("The world.. has fallen to darkness.. \n");
		printf("The dragon slayer, himself, has-- ughh.. leaving the world in peril for eternity.\n");
		printf("GAME OVER!\n");
		printf("Play again?\n");
		/*
		for(loop = 10; loop >= 0; loop++) {
			usleep(2 * 60);
			printf("%d...\n", loop);
			while(loop > 0) {

			}
		}
		*/
		//scanf("%c", command);
}

//end