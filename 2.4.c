/**
 * Exercise 2-4.
 * Write an alternative version of squeeze(s1,s2) that
 * deletes each character in s1 that matches any character in the string s2.
 *
 * Uncompiled. untested.
 */

#include <stdio.h>

 void squeeze(char string[], char character[]);

 void main()
 {
 	char string[64] = "CODE IS FOH WEAK";
 	char character[6] = "ASD";
 	squeeze(string, character);
 	printf("%s", string);

 	getchar();
 }

 void squeeze(char string[], char character[])
 {
 	int a, b, c;

 	// Iterate through each character of the string.
 	// Typical solutions iterate through the character while iterating over the string
 	// e.g. string[] = "CODE IS FOR WEAK"; character[] = "ASDC";
 	// #1 - [C]ODE IS FOR THE WEAK : [A]SDC
 	// #2 - [C]ODE IS FOR THE WEAK : A[S]DC
 	// This solution does an alternate algorithm
 	// #1 - [A]SDC - [C]ODE IS FOR THE WEAK
 	// #2 - [A]SDC - C[O]DE IS FOR THE WEAK
 	//
 	// Although my code "seems" to be wise and faster in given situations,
 	// what if character[] contains has more than the same character (more than 3 A: "ASDCAA").
 	//
 	// I don't have much time to think at the current time I'm writing this code, pardon the grammar, concept
 	// and shizz.
 	//
 	// I couldn't give the variables proper naming given the fact that I'm having trouble
 	// with .. *
 	for(a = b = 0; character[a] != '\0'; a++) {
 		for(c = 0; string[c] != '\0'; c++) {
 			if(string[c] != character[a]) {
 				string[b] = string[a];
 				b++;
 			}
 		}
 	}
 	string[b] = '\0';
 }
