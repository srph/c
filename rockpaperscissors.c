#include <stdio.h>
#include <conio.h>
#include <ctype.h>
#include <string.h>

main()
{
	char c;
	int i;
	int running = 1;

	// Define:
	// i = 1
	// running = 1
	// alarmMessage = 1
	// inputValid = 1
	// Do while `running` is true
	do {
		printf("[R]ock \n");
		printf("[P]aper \n");
		printf("[S]cissors \n");
		char inputValid = 1;
		char input[2];
		
		// While i is less than 2
		for(i = 1; i <= 2; i++) {
			// Do while `inputValid` is true
			do {
				printf("Player %d Move: ", i);
				// Scan user's move
				input[i - 1] = toupper(getch());
				printf("*\n");
				// If input is not either S, R, or P
				// Set inputValid to zero
				switch(input[i-1]) {
					case 'S':
					case 'P':
					case 'R':
						inputValid = 1;
						break;
					default:
						inputValid = 0;
						break;
				}
			} while(!inputValid);
		}


		printf("\n");
		int alarmMessage = 1;
		// Process Flowchart:
		// p1m = input[0];
		// p2m = input[1];
		char p1m = input[0];
		char p2m = input[1];

		// If p1m = 'R' AND p2m = 'S' OR
		// p1m = 'S' AND p2m = 'P' OR
		// p1m = 'P' AND p2m = 'R'
		if((p1m == 'R' && p2m == 'S') ||
			(p1m == 'S' && p2m == 'P') ||
			(p1m == 'P' && p2m == 'R')) {
			// Display p1 wins
			printf("Player 1 wins");

		// If p2m = 'R' AND p1m = 'S' OR
		// p2m = 'S' AND p1m = 'P' OR
		// p2m = 'P' AND p1m = 'R'
		} else if((p2m == 'R' && p1m == 'S') ||
			(p2m == 'S' && p1m == 'P') ||
			(p2m == 'P' && p1m == 'R')) {
			// Display p2 wins
			printf("Player 2 wins");
		} else {
			// Otherwise, set alarmMessage to zero
			alarmMessage = 0;
			// Display tie
			printf("Tie");
		}

		if(alarmMessage) {
			printf("\n");
			// If p1m == 'R' AND p2m == 'S' OR
			// p2m == 'R' ANDp1m == 'S'
			if((p1m == 'R' && p2m == 'S') ||
				(p2m == 'R' && p1m == 'S')) {
				// Display:
				printf("Rock breaks scissors");

			// Else If p2m == 'R' AND p1m == 'S' OR
			// p1m == 'R' AND p1m == 'S'
			} else if((p1m == 'S' && p2m == 'P') ||
				(p2m == 'S' && p1m == 'P')) {
				// Display:
				printf("Scissors cuts paper");

			// Else If p1m == 'P' and p2m == 'R' OR
			// p2m == 'P' && p1m == 'R'
			} else if((p1m == 'P' && p2m == 'R') ||
				(p2m == 'P' && p1m == 'R')) {
				// Display:
				printf("Paper covers rock");
			}
		}

		// Do while `inputValid` is zero or false
		do {
			// Set `inputValid` to 1 or true
			inputValid = 1;
			printf("\n");
			printf("Would you like to play again? ");
			char playAgain;
			// Input `playAgain`
			scanf(" %c", &playAgain);
			// If `playAgain` is equal to y
			// Set `running` to one or true
			// Otherwise,
			// Set `running` to zero or false
			switch(toupper(playAgain)) {
				case 'Y':
					memset(input, 0, sizeof(input));
					running = 1;
					break;
				default:
					running = 0;
					break;
			}
		} while(!inputValid);
	} while(running);

	getch();
	return 0;
}