/**
 * GWA Calculator v1.1
 *
 * This application currenlt follows the standard formula:
 * GWA Σ (grade * unit = subject final grade) / units
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify,
 * merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies
 * or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define bool int
#define TRUE 1
#define FALSE 0

bool outOfRange(float resource, float min, float max);
float computeGWA(float grades[], float units[], int subjects);

void main()
{
	float grades[15] = { 0 };	// This is where the grade of its respective subject is stored
	float units[15] = { 0 };	// Used to store the unit weight of its respective subject.
	float average;	// Where the GWA is stored after computation

	int subjects; // Ask the user for the number of subjects to compute
	int index;	// Index counter

	// Ask the user for the number of grades to compute
	do {
		printf("How many subjects would you like to calculate for the average? ");
		printf("\n");
		scanf("%d", &subjects);
	} while(outOfRange(subjects, 1, 11));

	printf("\n\n");

	// Input respective grade and unit for each subject
	for(index = 0; index <= subjects; index++) {
		do {
			printf("Enter grade (%d of %d): ", index, subjects);
			scanf("%f", &grades[index]);
			printf("\n");
		} while(!isdigit(grades[index]) && outOfRange(grades[index], 1.0, 5.0));

		do {
			printf("Enter its units (%d of %d): ", index, subjects);
			scanf("%f", &units[index]);
			printf("\n");
		} while(!isdigit(units[index]) && outOfRange(units[index], 1.0, 5.0));
	}

	for(index = 0; index <= subjects; index++) {
        printf("\n");
		printf("#%d", index + 1);
		printf("\t");
		printf("%f", grades[index]);
		printf("\t\t");
		printf("%f", units[index]);
	}

	printf("\n\n");

	average = computeGWA(grades, units, subjects);
	printf("Your computed General Weighted Average is: %1.2f", average);
	getchar();
}

/**
 * Validate if given resources is less than the minimum
 * or greater than the maximum
 *
 * @param	float	grade
 * @param	int		min
 * @param	int		max
 * @return	bool
 */
bool outOfRange(float resource, float min, float max)
{
	return (resource >= min && resource <= max) ? FALSE : TRUE;
}

/**
 * Compute the average of the given grade and units
 *
 * @param	float	grades
 * @param	float	units
 * @param	int	subjects
 * @return	float
 */
float computeGWA(float grades[], float units[], int subjects)
{
	int index;	// Index counter
	float average = 0.0;
	float total_units = 0.0;	// Total units

	for(index = 0; index <= subjects; index++) {
		average += (grades[index] * units[index]);
		total_units += units[index];
	}

	return average / total_units;
}
