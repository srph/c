#include <stdio.h>
#include <ctype.h>

/**
 * Exercise 2-3
 * Write a function htoi(s), which converts a string of hexadecimal digits (including an optional 0x or 0X)
 * into its equivalent integer value. The allowable digits are 0 through 9, a through f, and A through F.
 */

unsigned int htoi(char hex[])
{
	long result = 0, position;

	if( hex[0] == '0' && (hex[1] == 'x' || hex[1] == 'X') ) {
		// If it has the optional 0x or 0X,
		// start index 2
		position = 2;
	} else {
		// Otherwise, start at index 0
		position = 0;
	}

	// While the hex is not equal to the null terminator
	while(hex[position]) {
		result *= 16;
		
		// If the current position is at least 0 - 9
		if ( hex[position] >= '0' && hex[position] <= '9' )  {
			result += (hex[position] - '0');
		} else if ( hex[position] >= 'a' && hex[position] <= 'f' ) {
			result += (10 + hex[position] - 'a');
		} else if ( hex[position] >= 'A' && hex[position] <= 'F') {
			result += (10 + hex[position] - 'A');
		} else {
			// Simply return a zero
			return 0;
		}

		// Move on to the next position
		position++;
	}

	return result;
}

main()
{
	printf("DEAD -- %10u\n", htoi("DEAD"));
	printf("FF -- %10u\n", htoi("FF"));
	printf("FADE -- %10u\n", htoi("FADE"));
	printf("0xFADE -- %10u\n", htoi("0xFADE"));
	return 0;
}
