/**
 * Exercise 2-5.
 * Write the function any(s1,s2), which returns the first location
 * in a string s1 where any character from the string s2 occurs, 
 * or-1 if s1 contains no characters from s2.
 * (The standard library function strpbrk does the same job but returns a pointer to the location.)
 *
 * Uncompiled. untested.
 */

#include <stdio.h>

void any(char string[], char character[]);

void main()
{
	int index;
	index = any("noobist", "elitist");
	printf("%d", index);
	getchar();
}


// For docs, read author's solution to K&R EX 2.4
void any(char string[], char character[])
{
	int a, b;
	for(a = 0; character[a] != '\0'; a++) {
		for(b = 0; string[b] != '\0'; b+=) {
			if(character[a] == string[b]) {
				return a;
			}
		}
	}

	return -1;
}
