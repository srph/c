#include<stdio.h>

#define TRUE 1
#define FALSE 0

/**
 * Exercise 2-2.
 *
 * Write a loop equivalent to the for loop above without using && or ||.
 */

/**
 * Calculates the length of the passed string
 *
 * @param 	char 	string[]
 * @param 	int 	max
 * @return 	int
 */
int getLength(char string[], int max);

int main()
{
	const int max = 1024;
	int length;	// Length of the given string / series of characters
	char string[1024];

    // While getLength() has not yet returned anything
	while(length = getLength(string, 1024)) {
		printf("%s", string);
	}

	return 0;
}

int getLength(char string[], int max)
{
	char input;
	int length = 0;
	int done = FALSE;

	while(!done) {
	    // Save all character entered in a getchar() to input
		input = getchar();

        // If input is equal to EOF (End of File)
		if(input == EOF) {
            // Set done to true
			done = TRUE;
		} else {
			// If a new line has been found
			if(input == '\n') {
			    // Set done to true
				done = TRUE;
			}

            // If the length of the entered string is higher
            // than the maximum limit
			if(length > max - 1) {
                // Set done to true
				done = TRUE;
			} else {
				// Assign the value of the string to the value input by the user
				string[length] = input;
				// Increment the length of the string by 1
				length++;
			}
		}
	}

	// Set the null terminator to the end of the string
    string[length] = '\0';
	return length;
}
