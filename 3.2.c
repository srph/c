/**
 * Exercise 3-2. Write a function escape(s,t) that converts characters like newline
 * and tab into visible escape sequences like \n and \t as it copies the string t to s.
 * Use a switch. Write a function for the other direction as well,
 * converting escape sequences into the real characters.
 */

#include <stdio.h>

#define MAX 500

void escape(char input[], char escaped[]);

main()
{
	char string[MAX] = { 0 };
	char escaped[MAX] = { 0 };
	int i, c;

	for(i = 0; (c = getchar()) != EOF && i < MAX; i++) {
		string[i] = c;
	}

	escape(string, escaped);
	printf("%s", escaped);

	return;
}

void escape(char string[], char escaped[])
{
	int i, j;
	for(i = j = 0; string[i] != '\0' && j < MAX; i++, j++) {
		switch(string[i]) {
			case '\t':
				escaped[j] = '\\';
				escaped[++j] = 't';
				break;
			case '\n':
				escaped[j] = '\\';
				escaped[++j] = 'n';
				break;
			default:
				escaped[j] = string[i];
				break;
		}
	}

	escaped[j] = '\0';
}